/*!
 * ${copyright}
 */

sap.ui.define(["jquery.sap.global", "sap/m/InputRenderer"],
	function(jQuery, InputRenderer) {
	"use strict";

	/**
	 * MultiInputDialogRenderer renderer.
	 * @static
	 * @namespace
	 */
	var DeptPickerRenderer = InputRenderer.extend("newtype.sap.generic.DeptPickerRenderer");
	
	return DeptPickerRenderer;

}, /* bExport= */ true);