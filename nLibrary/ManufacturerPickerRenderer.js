/*!
 * ${copyright}
 */

sap.ui.define(["jquery.sap.global", "sap/m/InputRenderer"],
	function(jQuery, InputRenderer) {
	"use strict";

	/**
	 * MultiInputDialogRenderer renderer.
	 * @static
	 * @namespace
	 */
	var ManufacturerPickerRenderer = InputRenderer.extend("newtype.sap.generic.ManufacturerPickerRenderer");
	
	return ManufacturerPickerRenderer;

}, /* bExport= */ true);