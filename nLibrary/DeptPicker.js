sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/TableSelectDialog",
	"sap/m/Input",
    "sap/m/MessageToast",
	"sap/ui/model/Filter"

], function (Control, TableSelectDialog, Input, MessageToast, Filter) {
	"use strict";
	var DeptPicker = Input.extend("newtype.sap.generic.DeptPicker", {
		
		metadata : {
			properties : {
				"dialogSelectedKey" : {type : "string", defaultValue : null},
				"selectDialogTitle" : {type : "string", defaultValue : null},
				"selectDialogNoDataText" : {type : "string", defaultValue : null},
				"selectDialogContentHeight" : {type : "sap.ui.core.CSSSize", defaultValue : null}
			},
			aggregations : {
				__selectDialog : {type : "sap.m.TableSelectDialog", multiple: false, visibility : "hidden"},
				selectDialogItems : {type : "sap.ui.core.Item", multiple : false, singularName : "selectDialogItem"}
			},
			events : {},
			renderer : null
		},
		init : function () {
			Input.prototype.init.call(this);
			
			this.attachValueHelpRequest(this.__onValueHelpRequest, this);
			this.setAggregation("__selectDialog", new TableSelectDialog({
				title: this.getSelectDialogTitle(),
				noDataText: this.getSelectDialogNoDataText(),
				columns: [
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>deptIdTitle}"
			            })
			        }),
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>deptNameTitle}"
			            })
			        })
			    ]
				// EVENTS
				// search: this.__onValueHelpSearch.bind(this),
				// confirm: this.__onValueHelpConfirm.bind(this),
				// cancel: this.__onValueHelpConfirm.bind(this)
			}));
			
		},
		
		__onValueHelpConfirm: function(oEvent) {
			//var mode = this.getSelectDialogMode();
			var selectedContext = oEvent.getParameter("selectedContexts");
			var selectText = selectedContext.map(function(oContext) { return oContext.getObject().Me002; }).join(", ");
    		var selectKey = selectedContext.map(function(oContext) { return oContext.getObject().Me001; }).join(", ");
			
			this.setValue(selectText);
			this.setDialogSelectedKey(selectKey);
			// MessageToast.show(selectText + "||" + selectKey);
		},

		__onValueHelpRequest: function(oEvent) {
			var __oDeptList = new sap.ui.model.json.JSONModel("/ERP/api/cmsme");
			
			var selectDialog = this.getAggregation("__selectDialog");
			selectDialog.setTitle(this.getProperty("selectDialogTitle"));
			selectDialog.setNoDataText(this.getProperty("selectDialogNoDataText"));
			
			this.inputId = oEvent.getSource().getId();
			var oTemplate = new sap.m.ColumnListItem({
					cells:[
						new sap.m.Text({text:"{DeptList>Me001}"}),
						new sap.m.Text({text:"{DeptList>Me002}"})
					]
			});
			selectDialog.setModel(__oDeptList,"DeptList");
			selectDialog.bindItems("DeptList>/", oTemplate);
			
			selectDialog.attachSearch(this.__onValueHelpSearch,this);
			selectDialog.attachConfirm(this.__onValueHelpConfirm,this);
			selectDialog.attachCancel(this.__onValueHelpConfirm,this);
			selectDialog.open();
		},
		__onValueHelpSearch: function(oEvent) {
			var sSearchValue = oEvent.getParameter("value");

			var oFilter = new Filter(
				"Me002" ,
				sap.ui.model.FilterOperator.Contains, sSearchValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		}
	});
	
	// DeptPicker.prototype.getSelectDialogTitle = function () {
	// 	return this.getProperty("selectDialogTitle");
	// };
	return DeptPicker;
});