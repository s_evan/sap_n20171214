sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/TableSelectDialog",
	"sap/m/Input",
	"sap/ui/model/Filter"

], function (Control, TableSelectDialog, Input, Filter) {
	"use strict";
	var ManufacturerPicker = Input.extend("newtype.sap.generic.ManufacturerPicker", {
		
		metadata : {
			properties : {
				"supplier": {type : "string", defaultValue : null},
				"currency": {type : "string", defaultValue : null},
				"dialogSelectedKey" : {type : "string", defaultValue : null},
				"selectDialogTitle" : {type : "string", defaultValue : null},
				"selectDialogNoDataText" : {type : "string", defaultValue : null},
				"selectDialogContentHeight" : {type : "sap.ui.core.CSSSize", defaultValue : null}
			},
			aggregations : {
				__selectDialog : {type : "sap.m.TableSelectDialog", multiple: false, visibility : "hidden"}
			},
			events : {},
			renderer : null
		},
		init : function () {
			Input.prototype.init.call(this);
			
			this.attachValueHelpRequest(this.__onValueHelpRequest, this);
			this.setAggregation("__selectDialog", new TableSelectDialog({
				title: this.getSelectDialogTitle(),
				noDataText: this.getSelectDialogNoDataText(),
				columns: [
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>manuCode}"
			            })
			        }),
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>manuNo}"
			            })
			        }),new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>currency}"
			            })
			        }),
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>manuName}"
			            })
			        })
			    ]
				// EVENTS
				// search: this.__onValueHelpSearch.bind(this),
				// confirm: this.__onValueHelpConfirm.bind(this),
				// cancel: this.__onValueHelpConfirm.bind(this)
			}));
			
		},
		
		__onValueHelpConfirm: function(oEvent) {
			//var mode = this.getSelectDialogMode();
			var selectedContext = oEvent.getParameter("selectedContexts");
			var selectText = selectedContext.map(function(oContext) { return oContext.getObject().Ma003; }).join(", ");
    		var selectKey = selectedContext.map(function(oContext) { return oContext.getObject().Ma005; }).join(", ");
			var selectSupplier = selectedContext.map(function(oContext) { return oContext.getObject().Ma001; }).join(", ");
    		var selectCurrency = selectedContext.map(function(oContext) { return oContext.getObject().Ma021; }).join(", ");
			
			this.setValue(selectText);
			this.setDialogSelectedKey(selectKey);
			this.setSupplier(selectSupplier);
			this.setCurrency(selectCurrency);
			// MessageToast.show(selectText + "||" + selectKey);
		},

		__onValueHelpRequest: function(oEvent) {
			var __oMaList = new sap.ui.model.json.JSONModel("/ERP/api/purma");
			
			var selectDialog = this.getAggregation("__selectDialog");
			selectDialog.setTitle(this.getProperty("selectDialogTitle"));
			selectDialog.setNoDataText(this.getProperty("selectDialogNoDataText"));
			
			this.inputId = oEvent.getSource().getId();
			var oTemplate = new sap.m.ColumnListItem({
					cells:[
						new sap.m.Text({text:"{ManufactureList>Ma001}"}),
						new sap.m.Text({text:"{ManufactureList>Ma003}"}),
						new sap.m.Text({text:"{ManufactureList>Ma005}"}),
						new sap.m.Text({text:"{ManufactureList>Ma021}"})
					]
			});
			selectDialog.setModel(__oMaList,"ManufactureList");
			selectDialog.bindItems("ManufactureList>/", oTemplate);
			
			selectDialog.attachSearch(this.__onValueHelpSearch,this);
			selectDialog.attachConfirm(this.__onValueHelpConfirm,this);
			selectDialog.attachCancel(this.__onValueHelpConfirm,this);
			selectDialog.open();
		},
		__onValueHelpSearch: function(oEvent) {
			var sSearchValue = oEvent.getParameter("value");

			var oFilter = new Filter(
				"Ma003" ,
				sap.ui.model.FilterOperator.Contains, sSearchValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		}
	});
	
	return ManufacturerPicker;
});