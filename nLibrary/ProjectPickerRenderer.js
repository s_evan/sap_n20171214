/*!
 * ${copyright}
 */

sap.ui.define(["jquery.sap.global", "sap/m/InputRenderer"],
	function(jQuery, InputRenderer) {
	"use strict";

	/**
	 * MultiInputDialogRenderer renderer.
	 * @static
	 * @namespace
	 */
	var ProjectPickerRenderer = InputRenderer.extend("newtype.sap.generic.ProjectPickerRenderer");
	
	return ProjectPickerRenderer;

}, /* bExport= */ true);