sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/TableSelectDialog",
	"sap/m/Input",
	"sap/ui/model/Filter"

], function (Control, TableSelectDialog, Input, Filter) {
	"use strict";
	var ProjectPicker = Input.extend("newtype.sap.generic.ProjectPicker", {
		
		metadata : {
			properties : {
				"selectDialogTitle" : {type : "string", defaultValue : null},
				"selectDialogNoDataText" : {type : "string", defaultValue : null},
				"selectDialogContentHeight" : {type : "sap.ui.core.CSSSize", defaultValue : null}
			},
			aggregations : {
				__selectDialog : {type : "sap.m.TableSelectDialog", multiple: false, visibility : "hidden"},
				selectDialogItems : {type : "sap.ui.core.Item", multiple : false, singularName : "selectDialogItem"}
			},
			events : {},
			renderer : null
		},
		init : function () {
			Input.prototype.init.call(this);
			
			this.attachValueHelpRequest(this.__onValueHelpRequest, this);
			this.setAggregation("__selectDialog", new TableSelectDialog({
				title: this.getSelectDialogTitle(),
				noDataText: this.getSelectDialogNoDataText(),
				columns: [
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>deptIdTitle}"
			            })
			        }),
			        new sap.m.Column({
			            hAlign: "Begin",
			            header: new sap.m.Label({
			                text: "{i18n>deptNameTitle}"
			            })
			        })
			    ]
			}));
			
		},
		
		__onValueHelpConfirm: function(oEvent) {
			//var mode = this.getSelectDialogMode();
			var selectedContext = oEvent.getParameter("selectedContexts");
			var selectText = selectedContext.map(function(oContext) { return oContext.getObject().Nb001; }).join(", ");
    		
			this.setValue(selectText);
		},

		__onValueHelpRequest: function(oEvent) {
			var __oProjectList = new sap.ui.model.json.JSONModel("/ERP/api/cmsnb");
			
			var selectDialog = this.getAggregation("__selectDialog");
			selectDialog.setTitle(this.getProperty("selectDialogTitle"));
			selectDialog.setNoDataText(this.getProperty("selectDialogNoDataText"));
			
			this.inputId = oEvent.getSource().getId();
			var oTemplate = new sap.m.ColumnListItem({
					cells:[
						new sap.m.Text({text:"{ProjectList>Nb001}"}),
						new sap.m.Text({text:"{ProjectList>Nb002}"})
					]
			});
			selectDialog.setModel(__oProjectList,"ProjectList");
			selectDialog.bindItems("ProjectList>/", oTemplate);
			
			selectDialog.attachSearch(this.__onValueHelpSearch,this);
			selectDialog.attachConfirm(this.__onValueHelpConfirm,this);
			selectDialog.attachCancel(this.__onValueHelpConfirm,this);
			selectDialog.open();
		},
		__onValueHelpSearch: function(oEvent) {
			var sSearchValue = oEvent.getParameter("value");

			var oFilter = new Filter(
				"Nb002" ,
				sap.ui.model.FilterOperator.Contains, sSearchValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		}
	});
	
	// DeptPicker.prototype.getSelectDialogTitle = function () {
	// 	return this.getProperty("selectDialogTitle");
	// };
	return ProjectPicker;
});