sap.ui.define([], function(JSONModel, Device) {
	"use strict";

	return {
        toUpper: function(sValue){
            return sValue && sValue.toUpperCase();
        },
        formatDateTime : function (sdate) {
		    var oDate = new Date(sdate);
		    var _YY = oDate.getFullYear();
		    var _MM = "00" + oDate.getMonth() + 1;
		    var _DD = "00" + oDate.getDate();
		    var _hh = "00" + oDate.getHours();
		    var _mm = "00" + oDate.getMinutes();
		    var _ss = "00" + oDate.getSeconds();
		    var _now = "" + _YY + "/" +  _MM.substr( _MM.length - 2 , 2 ) + "/" +  _DD.substr(_DD.length - 2,2) + " " + _hh.substr(_hh.length - 2,2) + ":" + _mm.substr(_mm.length - 2,2) + ":" + _ss.substr(_ss.length - 2,2);
		    return _now;
		},
		formatToNumber : function(sValue){
			var sPattern = /\,/;
			return sValue.replace(sPattern,"");
		},
		processStep : function(sValue){
			var sPattern = /\]+\@?\[+/;
			var oProcessStep = sValue.split(sPattern);
			return oProcessStep[1];
		},
		processSteper : function(sValue){
			var sPattern = /\]+\@?\[+/;
			var oProcessStep = sValue.split(sPattern);
			return oProcessStep[2];
		}
	};
});