sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
	"sap/ui/Device",
    "sap/m/MessageToast"
], function(Controller,UIComponent,Device,MessageToast) {
	"use strict";

	return Controller.extend("nFlow.controller.Diagram", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nFlow.view.Diagram
		 */
		onInit: function() {
			this._oRouter = UIComponent.getRouterFor(this);
			var that = this;
			$.ajax({
			    url:"/Flow7/api/diagram",
			    type:"GET", //GET
			    dataType:"json",    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(data,textStatus,jqXHR){
			    	var oDiagramList = [];
					for (var i = 0; i < data.length; i++) {
				    	oDiagramList.push({"FolderGuid":data[i].FolderGuid,"FolderName":data[i].FolderName});
				    	for (var j = 0; j < data[i].DiagramLists.length; j++) {
					    	delete data[i].DiagramLists[j].XmlFile;
						}
					}
					var oDiagram = new sap.ui.model.json.JSONModel(oDiagramList);
					that.getView().setModel(oDiagram,"Diagram");
					
		    		jQuery.sap.require("jquery.sap.storage");
					var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
					oStorage.put("DiagramList",data);
			        //window.localStorage.setItem("DiagramList", JSON.stringify(data));
			    },
			    error:function(xhr,textStatus){
					MessageToast.show("错误");
			    },
			    complete:function(){
			    	
			    }
			});
		},
		diagramListFactory : function(sId,oContext) {
			var oUIControl = null;
			var sFolderName = oContext.getProperty("FolderName");

			oUIControl = new sap.m.ObjectListItem(sId, {
				title : sFolderName
			});
			if (oContext.getProperty("FolderGuid") === "FAMF") {
				oUIControl.setType("Navigation");
				oUIControl.attachPress(this.onPressGoToFold, this);
			} else {
				oUIControl.addStyleClass("data-nt-useful");
			}
			return oUIControl;
		},
		onPressGoToFold:function(oEvent){
			var oObject = oEvent.getSource().getBindingContext("Diagram");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			
    		jQuery.sap.require("jquery.sap.storage");
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			
			var oDiagramList = oStorage.get("DiagramList");
			for (var i = 0; i < oDiagramList.length; i++) {
				if(oDiagramList[i].FolderGuid === oItem.FolderGuid){
			        //window.localStorage.setItem("FoldList", JSON.stringify(oDiagramList[i].DiagramLists));
			        oStorage.put("FoldList", oDiagramList[i].DiagramLists);
				}
			}
			this._oRouter.navTo("applyFolder", {
				foldId : oItem.FolderGuid,
                 query:{
                 	folderName: oItem.FolderName
                 }
			}, true);
		}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nFlow.view.Diagram
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nFlow.view.Diagram
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nFlow.view.Diagram
		 */
		//	onExit: function() {
		//
		//	}

	});

});