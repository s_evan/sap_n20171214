sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
	"sap/ui/core/routing/History",
	"sap/ui/Device",
	"sap/ui/model/Filter",
    "sap/m/MessageToast"
], function(Controller,UIComponent,History,Device,Filter,MessageToast) {
	"use strict";

	return Controller.extend("nFlow.controller.ApplyFolder", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nFlow.view.ApplyFolder
		 */
		onInit: function() {
            this._oRouter = UIComponent.getRouterFor(this);
            this._oRouter.getRoute("applyFolder").attachPatternMatched(this._handleRouteMatched, this); 
            this._oRouter.getRoute("applyForm").attachPatternMatched(this._handleRouteMatched, this); 
        },
		
		folderListFactory : function(sId,oContext) {
			var oUIControl = null;
			var sDisplayName = oContext.getProperty("DisplayName");

			oUIControl = new sap.m.ObjectListItem(sId, {
				title : sDisplayName
			});
			if (oContext.getProperty("DiagramId") === "FDP_P0") {
				oUIControl.setType("Navigation");
				oUIControl.attachPress(this.onPressToApply, this);
			} else {
				oUIControl.addStyleClass("data-nt-useful");
			}
			return oUIControl;
		},

        onBackPress: function(oEvent) {
			sap.ui.getCore().getEventBus().publish("reSetDetailPage","reLoad");  
            this._oRouter.navTo("diagram");
        },
        onPressToApply:function(oEvent){
			var oObject = oEvent.getSource().getBindingContext("FoldList");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("applyForm", {
				foldId : this._sFoldId,
				diagramId : oItem.DiagramId,
                 query:{
                 	folderName: this._sFoldName,
                 	displayName: oItem.DisplayName,
                 	identify: oItem.Identify
                 }
			}, true);
        },
        _handleRouteMatched: function (oEvent) {    
    		jQuery.sap.require("jquery.sap.storage");        
            this._sFoldId = oEvent.getParameter("arguments").foldId;
            this._sFoldName = oEvent.getParameter("arguments")["?query"].folderName;
            
			this.getView().byId("ApplyFolder").setTitle(this._sFoldName);
			
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oFoldList = oStorage.get("FoldList");
			// var oFoldList = JSON.parse(window.localStorage.getItem("FoldList"));
			
			var oDiagramList = new sap.ui.model.json.JSONModel(oFoldList);
			this.getView().setModel(oDiagramList,"FoldList");
        }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nFlow.view.ApplyFolder
		 */
		//	onExit: function() {
		//
		//	}

	});

});