sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
	"sap/ui/core/Fragment",
    "sap/m/MessageToast",
	"sap/ui/model/Filter",
    "nFlow/model/Formatter"
], function(Controller,UIComponent,Fragment,MessageToast,Filter,Formatter)  {
	"use strict";

	return Controller.extend("nFlow.controller.FDP_P0", {
		oFormatter: Formatter, 
		_formFragments: {},
		inputId: "",
		sDialogParameter : "Me002",
		sFormId: null,

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nFlow.view.ApplyFLow
		 */
		onInit: function() {
            // this._oRouter = UIComponent.getRouterFor(this);
            // this._oRouter.getRoute("applyForm").attachPatternMatched(this._handleRouteMatched, this); 
            // window.localStorage.clear();
			sap.ui.getCore().getEventBus().subscribe("sendMessage2Form","SaveDraft", this._doSomething, this); 
			sap.ui.getCore().getEventBus().subscribe("sendMessage2Form","SubmitForm", this._doSomething, this); 
            
            this._islocalStorage("FDP_P0");
		},
		
		_doSomething:function(sChannelId, sEventId, oData){
			var oFormDetail = this.getView().getModel("Detail").getJSON();
			sap.ui.getCore().getEventBus().publish("sendFrame",sEventId,oFormDetail);  
		},
		// onExit : function () {
		// 	for (var sPropertyName in this._formFragments) {
		// 		if (!this._formFragments.hasOwnProperty(sPropertyName)) {
		// 			return;
		// 		}

		// 		this._formFragments[sPropertyName].destroy();
		// 		this._formFragments[sPropertyName] = null;
		// 	}
		// },
        _handleRouteMatched: function (oEvent) {            
            var sFoldId = decodeURIComponent(oEvent.getParameter("arguments").foldId);
			// this.getView().byId("ApplyPage").setTitle(oEvent.getParameter("arguments")["?query"].displayName);
			this.sFormId = oEvent.getParameter("arguments").diagramId;
            this._islocalStorage(this.sFormId);
        },

		_getFormFragment: function (sFragmentName) {
			var oFormFragment = this._formFragments[sFragmentName];

			if (oFormFragment) {
				return oFormFragment;
			}
			oFormFragment = sap.ui.xmlfragment(this.getView().getId(),  "nFlow.view.fragment.flowPages." + sFragmentName);

			this._formFragments[sFragmentName] = oFormFragment;
			return this._formFragments[sFragmentName];
		},

		_showFormFragment : function (sFragmentName) {
			var oPage = this.getView().byId("ApplyPage");

			oPage.removeAllContent();
			oPage.insertContent(this._getFormFragment(sFragmentName));
			
			// this.getView().byId("ApplicantDeptName").attachValueHelpRequest(this.handleDeptValueHelp,this);
			// this.getView().byId("PaySupplier").attachValueHelpRequest(this.handlePayValueHelp,this);
			// this.getView().byId("ProjectCode").attachValueHelpRequest(this.handleProjectValueHelp,this);
			
			// this.getView().byId("DocumentCategory").attachChange(this.handleInvoiceChange,this);
			this._loadReceiptDetail();
		},

    	_islocalStorage:function(sDiagramId){
    		// var oFlowPDF = window.localStorage.getItem("Flow" + sDiagramId);
    		jQuery.sap.require("jquery.sap.storage");
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oData = oStorage.get("Flow2" + sDiagramId);
			//Check if there is data into the Storage
			if (oData) {
    			this._oFlowDetail = new sap.ui.model.json.JSONModel();
    			this._oFlowDetail.setJSON(oData);
				this.getView().setModel(this._oFlowDetail,"Detail");
			}else{
				this._oFlowDetail = new sap.ui.model.json.JSONModel("/Flow7/api/FDP");
				this.getView().setModel(this._oFlowDetail,"Detail");
			}
			
    		this._loadPayTypeList();
			this._loadReceiptDetail();
    	},
    	
		_loadPayTypeList:function(){
			var oPayTypeList = [
				{"typeName":"供应商货款","typeId":"Pay01"},
				{"typeName":"一般请款","typeId":"Pay02"},
				{"typeName":"暂付款","typeId":"Pay03"},
				{"typeName":"零用金","typeId":"Pay04"},
				{"typeName":"其他","typeId":"Pay05"}
			];

			this._oPayType = new sap.ui.model.json.JSONModel(oPayTypeList);
			this.getView().setModel(this._oPayType,"PayType");
		},
		
		_loadReceiptDetail:function(){
			// this._oReceiptDetail = new sap.ui.model.json.JSONModel("/ERP/api/receipt");
			// this.getView().setModel(this._oReceiptDetail,"ReceiptDetail");
			var that = this;
			$.ajax({
			    url:"/ERP/api/receipt",
			    type:"GET", //GET
			    dataType:"json",    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(data,textStatus,jqXHR){
			    	var _oReceiptDetail = [];
					for (var i = 0; i < data.length; i++) {
						var ReceiptItem = {
							"Receipt":data[i].Receipt,
							"ReceiptId":data[i].ReceiptId,
							"Invoice":[],
							"Tax":[]
						};
	    				for(var j = 0; j < data[i].Invoice.length; j ++){
	    					ReceiptItem.Invoice.push({"TypeName":data[i].Invoice[j]});
						}
						
	    				for(var x = 0; x < data[i].Tax.length; x ++){
	    					ReceiptItem.Tax.push({"TaxName":data[i].Tax[x]});
						}
				    	_oReceiptDetail.push(ReceiptItem);
					}
					var _oReceipt = new sap.ui.model.json.JSONModel(_oReceiptDetail);
					that.getView().setModel(_oReceipt,"ReceiptDetail");
			    },
			    error:function(xhr,textStatus){
					MessageToast.show("错误");
			    },
			    complete:function(){
					var oViewModelJson = JSON.parse(that.getView().getModel("Detail").getJSON()); 
					that.reSetInvoice(oViewModelJson.DocumentCategoryId);
			    }
			});
		},
		
	/*	handleDeptValueHelp : function (oEvent) {
			this._oDeptList = new sap.ui.model.json.JSONModel("/ERP/api/cmsme");
			this.getView().setModel(this._oDeptList,"DeptList");
			this.sDialogParameter = "Me002";
			this.inputId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpMeDialog) {
				this._valueHelpMeDialog = sap.ui.xmlfragment("nFlow.view.fragment.dialog.Dept",this);
				this.getView().addDependent(this._valueHelpMeDialog);
			}
			// open value help dialog
			this._valueHelpMeDialog.open();
		},
		handlePayValueHelp : function (oEvent) {
			this._oMaList = new sap.ui.model.json.JSONModel("/ERP/api/purma");
			this.getView().setModel(this._oMaList,"ManufactureList");
			this.sDialogParameter = "Ma003";
			this.inputId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpMaDialog) {
				this._valueHelpMaDialog = sap.ui.xmlfragment("nFlow.view.fragment.dialog.Manufacturer",this);
				this.getView().addDependent(this._valueHelpMaDialog);
			}
			// open value help dialog
			this._valueHelpMaDialog.open();
		},
		handleProjectValueHelp : function (oEvent) {
			this._oProjectList = new sap.ui.model.json.JSONModel("/ERP/api/cmsnb");
			this.getView().setModel(this._oProjectList,"ProjectList");
			this.sDialogParameter = "Nb001";
			this.inputId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpProjectDialog) {
				this._valueHelpProjectDialog = sap.ui.xmlfragment("nFlow.view.fragment.dialog.Project",this);
				this.getView().addDependent(this._valueHelpProjectDialog);
			}
			// open value help dialog
			this._valueHelpProjectDialog.open();
		},

		_handleValueHelpSearch : function (oEvent) {
			var sValue = oEvent.getParameter("value");
			
			var oFilter = new Filter(
				this.sDialogParameter ,
				sap.ui.model.FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},
		
		_handleValueHelpClose : function (oEvent) {
			var oViewModel = this.getView().getModel("Detail");  
			var sDialogId = oEvent.getSource().getId();
			
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				if(sDialogId === "DeptDialog"){
    				oViewModel.setProperty("/ApplicantDeptName", aContexts.map(function(oContext) { return oContext.getObject().Me002; }).join(", "));
    				oViewModel.setProperty("/ApplicantDept", aContexts.map(function(oContext) { return oContext.getObject().Me001; }).join(", "));
				}else if(sDialogId === "ManufacturerDialog"){
    				oViewModel.setProperty("/PaySupplierName", aContexts.map(function(oContext) { return oContext.getObject().Ma003; }).join(", "));
    				oViewModel.setProperty("/PaySupplierId", aContexts.map(function(oContext) { return oContext.getObject().Ma005; }).join(", "));
    				oViewModel.setProperty("/PaySupplier", aContexts.map(function(oContext) { return oContext.getObject().Ma001; }).join(", "));
    				oViewModel.setProperty("/Currency", aContexts.map(function(oContext) { return oContext.getObject().Ma021; }).join(", "));
				}else if(sDialogId === "ProjectDialog"){
    				oViewModel.setProperty("/ProjectCode", aContexts.map(function(oContext) { return oContext.getObject().Nb001; }).join(", "));
				}	
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		*/
		handleInvoiceChange:function(oEvent){
			var oViewModel = this.getView().getModel("Detail"); 
			var sCategoryId = oEvent.getParameter("selectedItem").getKey();
			oViewModel.setProperty("/DocumentCategory", oEvent.getParameter("selectedItem").getText());
			this.reSetInvoice(sCategoryId);
		},
		
		reSetInvoice:function(sCategoryId){
			var sReceiptDetail = JSON.parse(this.getView().getModel("ReceiptDetail").getJSON());
			
			for(var i = 0; i < sReceiptDetail.length; i ++){
				if(parseInt(sReceiptDetail[i].ReceiptId,0) === parseInt(sCategoryId,0) ){
					this._oInvoiceType = new sap.ui.model.json.JSONModel(sReceiptDetail[i].Invoice);
					this.getView().setModel(this._oInvoiceType,"InvoiceType");
					this._oInvoiceType.refresh();
					
					this._oTaxationType = new sap.ui.model.json.JSONModel(sReceiptDetail[i].Tax);
					this.getView().setModel(this._oTaxationType,"TaxationType");
					this._oTaxationType.refresh();
    			}
			}
		},
		
		handleChangeCalc:function(oEvent){
			var newValue = this.oFormatter.formatToNumber(oEvent.getParameter("value"));
			if(isNaN(newValue)){
				MessageToast.show("请输入数字...");
			}else{
				var nNoTaxAmount = this.oFormatter.formatToNumber(this.getView().byId("NoTaxAmount").getValue());
				var nTax = this.oFormatter.formatToNumber(this.getView().byId("Tax").getValue());
				var nTotal = parseFloat(nNoTaxAmount) + parseFloat(nTax); 
				var oViewModel = this.getView().getModel("Detail"); 
				
				oViewModel.setProperty("/Total", nTotal);
			}
		}

	});

});