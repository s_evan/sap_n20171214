sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nFlow.controller.App", {
		onInit: function() {
			sap.ui.getCore().getEventBus().subscribe("reSetDetailPage","reLoad", this._reSetDetailPage, this); 
		},
		_reSetDetailPage:function(){
			// this.getView().byId("Apply").removeDetailPage("processPage");
			// this.getView().byId("Apply").removeDetailPage("ApplyPage");
			this.getView().byId("Apply").removeAllDetailPages();
		}
	});
});