sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
	"sap/ui/core/Fragment",
    "sap/m/MessageToast",
	"sap/ui/model/Filter",
    "nFlow/model/Formatter"
], function(Controller,UIComponent,Fragment,MessageToast,Filter,Formatter) {
	"use strict";

	return Controller.extend("nFlow.controller.ApplyForm", {
		sFormId: null,

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nFlow.view.ApplyFLow
		 */
		onInit: function() {
            this._oRouter = UIComponent.getRouterFor(this);
            this._oRouter.getRoute("applyForm").attachPatternMatched(this._handleRouteMatched, this); 
            // window.localStorage.clear();
            //注册监听事件，接收MVC VIEW推送事件
			sap.ui.getCore().getEventBus().subscribe("sendFrame","SaveDraft", this._handleSave2Draft, this); 
			sap.ui.getCore().getEventBus().subscribe("sendFrame","SubmitForm", this._handleSubmitForm, this); 
		},

		// onExit : function () {
		// 	for (var sPropertyName in this._formFragments) {
		// 		if (!this._formFragments.hasOwnProperty(sPropertyName)) {
		// 			return;
		// 		}

		// 		this._formFragments[sPropertyName].destroy();
		// 		this._formFragments[sPropertyName] = null;
		// 	}
		// },
		
        _handleRouteMatched: function (oEvent) {            
            var sFoldId = decodeURIComponent(oEvent.getParameter("arguments").foldId);
			this.getView().byId("ApplyPage").setTitle(oEvent.getParameter("arguments")["?query"].displayName);
			this.sFormId = oEvent.getParameter("arguments").diagramId;
			
            var oFormView = new sap.ui.xmlview( "nFlow.view." + this.sFormId);
            
			// var oView = this.getView().byId( this.sFormId + "Form");
			// oView.destroy();
			var oPage = this.getView().byId("ApplyPage");
			oPage.removeAllContent();
			oPage.insertContent(oFormView);
        },
        
		handleSaveDraft:function(){
			sap.ui.getCore().getEventBus().publish("sendMessage2Form","SaveDraft");  
		},
		
		handleSavePress:function(){
			sap.ui.getCore().getEventBus().publish("sendMessage2Form","SubmitForm");  
		},
		
		_handleSave2Draft:function(sChannelId, sEventId, oData){
			jQuery.sap.require("jquery.sap.storage");
            var oFormDetail = jQuery.sap.storage(jQuery.sap.storage.Type.local);
            oFormDetail.put("Flow2" + this.sFormId, oData);
		},
		
		_handleSubmitForm:function(sChannelId, sEventId, oData){
			console.log(oData);
			$.ajax({
			    url: "/Flow7/api/fdp/m",
			    type: "POST", 
			    dataType: "json",
			    data: oData,
			    success:function(data,textStatus,jqXHR){
			    	if(data === "Ok"){
			    		$.ajax({
						    url: "/Flow7/api/fdp/s",
						    type: "POST", 
						    dataType: "json",
						    data: oData,
						    success:function(data, textStatus, jqXHR){
						    	if(data === "Ok"){
						    		MessageToast.show("表单已送出！");
						    	}
						    },
						    error:function(xhr, textStatus){
								MessageToast.show("错误");
						    }
						});
			    	}
			    },
			    error:function(xhr,textStatus){
					MessageToast.show("错误");
			    },
			    complete:function(){
		            var oFormDetail = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		            oFormDetail.remove("Flow2" + this.sFormId);
			    }
			});
		}
		
		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nFlow.view.ApplyFLow
		 */
		//	onExit: function() {
		//
		//	}

	});

});